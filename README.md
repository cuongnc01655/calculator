#Author#

Cuong Nguyen
cuongnc01655@gmail.com

# What is it??? #

This is my Final Project for Android basic course, also it is my hello android project

#Function#
This app contain all classic calculator functions:

* Plus
* Subtract
* Multiply
* Divice
* Percent
* Memory Recall
* Memory Clean
* Memory Plus

# How to run it #

You can import project and continue develop it, or install **calculator.apk** from **dist** folder into your Android device

#Special Thanks#

* My tutor for guide me to do this
* My colleague help me test and review