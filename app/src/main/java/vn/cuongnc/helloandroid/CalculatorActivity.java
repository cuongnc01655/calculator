package vn.cuongnc.helloandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by cuong on 5/11/16.
 */
public class CalculatorActivity extends Activity {
    private TextView outputView;
    private TextView operationView;
    private TextView lastValView;
    private TextView memView;
    private BigDecimal lastValue = null;
    private int operation;
    private static String PATTERN = "^([+-]?\\\\d*\\\\.?\\\\d*)$";
    private static int MAX_CHAR = 15;
    private boolean needReset = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        outputView = (TextView) findViewById(R.id.output);
        operationView = (TextView) findViewById(R.id.operationView);
        lastValView = (TextView) findViewById(R.id.lastValView);
        memView = (TextView) findViewById(R.id.memView);
    }

    public void btnNumberClicked(View view) {
        //append into view, void 0 at beginning
        Button button = (Button) view;
        String output = outputView.getText().toString();
        if(needReset) {
            output = "";
            lastValue = null;
            needReset = false;
        }
        String btnContent = button.getText().toString();
        if (output.length() < MAX_CHAR) {
            String newVal = output + btnContent;
            if(newVal.equals("00"))
                newVal = "0";
            if (!newVal.matches(PATTERN)) {
//                newVal = getFormattedOutput(newVal);
                outputView.setText(newVal);

            } else {
                Log.w("btnNumberClicked","invalid input: " + newVal);
            }
        }
    }



    public void btnOperationClicked(View view) {
        String output = outputView.getText().toString();
        Button calBtn = (Button) view;
        //save operation
        operation = calBtn.getId();
        operationView.setText(calBtn.getText());
        //set needreset to false
        needReset = false;

        if(!output.isEmpty()) {
            lastValView.setText(outputView.getText());
            lastValue = new BigDecimal(output);
        }
        //clear view
        outputView.setText("");
    }

    public void btnClearClicked(View view) {
        outputView.setText("");
        lastValView.setText("");
        operationView.setText("");
        lastValue = new BigDecimal("0");
        operation = 0;

    }

    public void btnDotClicked(View view) {
        String output = outputView.getText().toString();
        if(needReset) {
            output = "";
            lastValue = null;
            needReset = false;
        }
        if(!output.contains(".")) {
            outputView.setText(output + ".");
        }
    }

    public void btnDelClicked(View view) {
        String output = outputView.getText().toString();
        if(output!= null && !output.isEmpty()) {
            String tmp = output.substring(0, output.length() - 1);
            if(tmp.equals("-") || tmp.equals(".")) {
                outputView.setText("");
            } else {
                outputView.setText(tmp);
            }

        }
    }

    public void btnPosNegClicked(View view) {
        String output = outputView.getText().toString();
        if(output != null && !output.isEmpty()) {
            if (output.startsWith("-")) {
                outputView.setText(output.substring(1));
            } else if(!output.equals("0")) {
                outputView.setText("-" + output);
            }
        }
    }

    public void btnPercentClicked(View view) {
        if(outputView.getText().toString().isEmpty()) return;
        BigDecimal currentValue = new BigDecimal(outputView.getText().toString());
        BigDecimal result = currentValue.divide(new BigDecimal("100"), 9, BigDecimal.ROUND_HALF_UP);
        displayResult(result);
    }

    public void doCalculator(View view) {
        if(outputView.getText().toString().isEmpty() || lastValue == null) return;
        BigDecimal currentValue = new BigDecimal(outputView.getText().toString());
        BigDecimal result;
        switch (operation) {
            case R.id.btnAdd:
                result = lastValue.add(currentValue, MathContext.DECIMAL32);
                break;
            case R.id.btnSub:
                result = lastValue.subtract(currentValue, MathContext.DECIMAL32);
                break;
            case R.id.btnMult:
                result = lastValue.multiply(currentValue, MathContext.DECIMAL32);
                break;
            case R.id.btnDiv:
                //avoid Division by Zero exception
                if(currentValue.compareTo(new BigDecimal(0)) == 0) {
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

                    dlgAlert.setMessage("Could not Divide by Zero");
                    dlgAlert.setTitle("Error");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    return;
                }

                result = lastValue.divide(currentValue,9, RoundingMode.HALF_UP);
                break;

            default:
                return;
        }
        displayResult(result);
    }

    public void displayResult(BigDecimal result) {
        operation = 0;
        operationView.setText("");
        lastValView.setText("");
        needReset = true;
        if(result.compareTo(BigDecimal.valueOf(Float.MAX_VALUE)) > 0)
            result = BigDecimal.valueOf(Float.MAX_VALUE);
        else if(result.compareTo(BigDecimal.valueOf(-Float.MAX_VALUE)) < 0)
            result = BigDecimal.valueOf(-Float.MAX_VALUE);
        String resultText = getFormattedOutput(result.toString());
        if(resultText.equals("-0")) resultText = "0";
        lastValue = new BigDecimal(resultText);
        outputView.setText(resultText);
    }

    private String getFormattedOutput(String input) {
        if(input == null) return  null;
        input = input.indexOf(".") < 0 ? input : input.replaceAll("0*$", "").replaceAll("\\.$", "");
        int maxLength = (input.length() < MAX_CHAR)?input.length():MAX_CHAR;
        input = input.substring(0, maxLength);
        return input;
    }

    public void btnMCClicked(View view) {
        memView.setText("");
    }

    public void btnMRClicked(View view) {
        outputView.setText(memView.getText());
    }

    public void btnMPlusClicked(View view) {
        memView.setText(outputView.getText());
    }

}
